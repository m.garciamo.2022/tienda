#!/usr/bin/env python3

'''
Maneja un diccionario con los artículos de una tienda y sus precios,
y permite luego comprar alguno de sus elementos, y ver el precio total
de la compra.
'''
import sys

articulos = {}

def anadir(articulo, precio):
    """Añade un artículo y un precio al diccionario artículos"""
    articulos[articulo] = precio

def mostrar():
    """Muestra en pantalla todos los artículos y sus precios"""
    print("Lista de artículos en la tienda:")
    for articulo, precio in articulos.items():
        print(f"{articulo}: {precio} euros")

def pedir_articulo() -> str:
    """Pide al usuario el artículo a comprar."""
    while True:
        articulo = input("Artículo: ")
        if articulo in articulos:
            return articulo
        else:
            print(f"El artículo '{articulo}' no está disponible en la tienda. Introduce uno válido.")

def pedir_cantidad() -> float:
    """Pide al usuario la cantidad a comprar."""
    while True:
        cantidad = input("Cantidad: ")
        try:
            cantidad = float(cantidad)
            return cantidad
        except ValueError:
            print("La cantidad debe ser un número válido.")

def main():
    """Toma los artículos declarados como argumentos en la línea de comando,
    junto con sus precios, almacénalos en el diccionario artículos mediante
    llamadas a la función añadir. Luego muestra el listado de artículos
    y precios para que el usuario pueda elegir. Termina llamando a las
    funciones pedir_articulo y pedir cantidad, y mostrando en pantalla la
    cantidad comprada, de qué artículo, y cuánto es su precio.
    """
    args = sys.argv[1:]
    if len(args) % 2 != 0:
        print("Error en argumentos: Cada artículo debe tener un precio.")
        sys.exit(1)

    for i in range(0, len(args), 2):
        articulo = args[i]
        precio = args[i + 1]
        try:
            precio = float(precio)
        except ValueError:
            print(f"Error en argumentos: '{precio}' no es un precio correcto.")
            sys.exit(1)
        anadir(articulo, precio)

    mostrar()

    articulo = pedir_articulo()
    cantidad = pedir_cantidad()

    precio_total = cantidad * articulos[articulo]
    print(f"Compra total: {cantidad} de {articulo}, a pagar {precio_total} euros")

if __name__ == '__main__':
    main()