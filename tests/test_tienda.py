#!/usr/bin/python3
# -*- coding: utf-8 -*-

import contextlib
import io
import os
import sys
import unittest
from unittest.mock import patch

import tienda

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')

expected = ["""Lista de artículos en la tienda:
leche: 2.0 euros
carne: 10.0 euros
pan: 0.75 euros

Artículo: Cantidad: 
Compra total: 10.0 de leche, a pagar 20.0
""",
"""Lista de artículos en la tienda:
leche: 2.0 euros
pan: 0.75 euros

Artículo: Cantidad: 
Compra total: 1.5 de pan, a pagar 1.125
"""
            ]


class TestAnadir(unittest.TestCase):

    def setUp(self):
        tienda.articulos = {}

    def test_simple(self):
        tienda.anadir('leche', 2.0)
        tienda.anadir('carne', 10.0)
        tienda.anadir('pan', 0.75)
        self.assertEqual(tienda.articulos, {'leche': 2.0, 'carne': 10.0, 'pan': 0.75})

    def test_simple2(self):
        tienda.anadir('leche', 1.0)
        self.assertEqual(tienda.articulos, {'leche': 1.0})


class TestMostrar(unittest.TestCase):

    def setUp(self):
        tienda.articulos = {}

    def test_mostrar(self):
        tienda.anadir('leche', 1.0)
        with contextlib.redirect_stdout(io.StringIO()) as o:
            tienda.mostrar()
            output = o.getvalue()
        self.assertEqual("leche: 1.0 euros\n", output)

    def test_mostrar2(self):
        tienda.anadir('leche', 1.0)
        tienda.anadir('pan', 0.75)
        with contextlib.redirect_stdout(io.StringIO()) as o:
            tienda.mostrar()
            output = o.getvalue()
        self.assertEqual("leche: 1.0 euros\npan: 0.75 euros\n", output)


class TestPedirArticulo(unittest.TestCase):

    def setUp(self):
        tienda.articulos = {'leche': 1.0, 'pan': 0.75}

    def test_simple(self):
        sys.stdin = io.StringIO("leche\n")
        articulo = tienda.pedir_articulo()
        self.assertEqual('leche', articulo)

    def test_bad_article(self):
        sys.stdin = io.StringIO("agua\nleche\n")
        articulo = tienda.pedir_articulo()
        self.assertEqual('leche', articulo)


class TestPedirCantidad(unittest.TestCase):

    def test_simple(self):
        sys.stdin = io.StringIO("1.0\n")
        cantidad = tienda.pedir_cantidad()
        self.assertEqual(1.0, cantidad)

    def test_int(self):
        sys.stdin = io.StringIO("1\n")
        cantidad = tienda.pedir_cantidad()
        self.assertEqual(1.0, cantidad)

    def test_bad_qunatity(self):
        sys.stdin = io.StringIO("error\n1.0\n")
        cantidad = tienda.pedir_cantidad()
        self.assertEqual(1.0, cantidad)


class TestMain(unittest.TestCase):

    def setUp(self):
        tienda.articulos = {}

    def test_simple(self):
        sys.stdin = io.StringIO("leche\n10\n")
        with patch.object(sys, 'argv', ['tienda.py', 'leche', '2.0', 'carne', '10', 'pan', '0.75']):
            with contextlib.redirect_stdout(io.StringIO()) as o:
                tienda.main()
                output = o.getvalue()
        self.assertEqual(expected[0], output)

    def test_simple2(self):
        sys.stdin = io.StringIO("pan\n1.5\n")
        with patch.object(sys, 'argv', ['tienda.py', 'leche', '2', 'pan', '0.75']):
            with contextlib.redirect_stdout(io.StringIO()) as o:
                tienda.main()
                output = o.getvalue()
        self.assertEqual(expected[1], output)

    def test_nothing(self):
        with patch.object(sys, 'argv', ['tienda.py']):
            with self.assertRaises(SystemExit):
                tienda.main()

    def test_one(self):
        with patch.object(sys, 'argv', ['tienda.py', 'leche']):
            with self.assertRaises(SystemExit):
                tienda.main()

    def test_missing_price(self):
        with patch.object(sys, 'argv', ['tienda.py', 'leche', '10', 'pan']):
            with self.assertRaises(SystemExit):
                tienda.main()


if __name__ == '__main__':
    unittest.main(module=__name__, buffer=True, exit=False)
